import React from "react";
import { todos as todosList } from "./components/to-do/todos";
import { useState } from "react";
import { v4 as uuid } from "uuid";
import { Switch, Route } from "react-router-dom";
import TodoList from "./components/to-do/todo-list";
import Navigation from "./navigation/navigation";
import { connect } from "react-redux";
import { addTodo, toggleClear } from "./components/action/todosActions";

function App(props) {
  const [inputText, setInputText] = useState("");

  const handleAddTodo = (event) => {
    if (event.which === 13) {
      props.addTodo(inputText);
      setInputText("");
    }
  };

  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          onChange={(event) => setInputText(event.target.value)}
          onKeyDown={(event) => handleAddTodo(event)}
          className="new-todo"
          value={inputText}
          placeholder="What needs to be done?"
          autoFocus
        />
      </header>

      <Switch>
        <Route exact path="/">
          <TodoList todos={Object.values(props.todos)} />
        </Route>

        <Route path="/active">
          <TodoList
            todos={Object.values(props.todos).filter((todo) => !todo.completed)}
          />
        </Route>
        <Route path="/completed">
          <TodoList
            todos={Object.values(props.todos).filter((todo) => todo.completed)}
          />
        </Route>
      </Switch>

      <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
        <Navigation />
        <button className="clear-completed" onClick={props.toggleClear}>
          Clear completed
        </button>
      </footer>
    </section>
  );
}

const mapStateToProps = (state) => ({
  todos: state.todos,
});
const mapDispatchToProps = (dispatch) => ({
  addTodo: (inputText) => dispatch(addTodo(inputText)),
  toggleClear: () => dispatch(toggleClear()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
