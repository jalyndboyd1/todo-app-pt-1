export const ADDTODO = "ADDTODO";

export const addTodo = (inputText) => ({
  type: ADDTODO,
  payload: { inputText },
});

export const TOGGLETODO = "TOGGLETODO";
export const toggleTodo = (id) => ({ type: TOGGLETODO, payload: { id } });
export const HANDLEREMOVE = "HANDLEREMOVE";
export const handleRemove = (id) => ({ type: HANDLEREMOVE, payload: { id } });
export const TOGGLECLEAR = "TOGGLECLEAR";
export const toggleClear = () => ({ type: TOGGLECLEAR });
