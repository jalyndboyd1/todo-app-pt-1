//Building a combined reducer here.
//Root reducer is combined reducer
import { combineReducers } from "redux";
import { todos } from "./todos";

export default combineReducers({
  todos,
});
