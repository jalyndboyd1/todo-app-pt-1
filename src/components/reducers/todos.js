import { todos as initialTodos } from "../to-do/todos";
import {
  ADDTODO,
  TOGGLETODO,
  HANDLEREMOVE,
  TOGGLECLEAR,
} from "../action/todosActions";

import { v4 as uuid } from "uuid";

export const todos = (state = initialTodos, action) => {
  switch (action.type) {
    case ADDTODO: {
      const newID = uuid();
      const newTodo = {
        userId: 1,
        id: newID,
        title: action.payload.inputText,
        completed: false,
      };
      return {
        ...state,
        [newID]: newTodo,
      };
    }
    case TOGGLETODO: {
      const newTodos = { ...state };
      const { id } = action.payload;
      newTodos[id].completed = !newTodos[id].completed;
      return newTodos;
    }
    case HANDLEREMOVE: {
      const newTodos = { ...state };
      delete newTodos[action.payload.id];
      return newTodos;
    }
    case TOGGLECLEAR: {
      const newTodos = { ...state };

      for (let todo in newTodos) {
        if (newTodos[todo].completed) {
          delete newTodos[todo];
        }
      }
      return newTodos;
    }
    default:
      return state;
  }
};
