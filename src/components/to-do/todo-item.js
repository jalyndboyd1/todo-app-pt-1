import React from "react";
import { useDispatch } from "react-redux";
import { toggleTodo, handleRemove } from "../action/todosActions";

function TodoItem(props) {
  const dispatch = useDispatch();
  return (
    <li className={props.completed ? "completed" : ""}>
      <div className="view">
        <input
          className="toggle"
          type="checkbox"
          checked={props.completed}
          onChange={() => dispatch(toggleTodo(props.id))}
        />
        <label>{props.title}</label>
        <button
          className="destroy"
          onClick={() => dispatch(handleRemove(props.id))}
        />
      </div>
    </li>
  );
}

export default TodoItem;
