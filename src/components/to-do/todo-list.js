import React from "react";
import TodoItem from "./todo-item";

function TodoList(props) {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo) => (
          <TodoItem
            title={todo.title}
            completed={todo.completed}
            id={todo.id}
            handleToggle={props.handleToggle}
            handleDelete={props.handleDelete}
            key={todo.id}
          />
        ))}
      </ul>
    </section>
  );
}

export default TodoList;
