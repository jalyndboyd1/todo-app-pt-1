import { Link } from "react-router-dom";
import React from "react";

function Navigation(props) {
  return (
    <div className="Navigation">
      <ul className="filters">
        <li className="all">
          <Link to="/">All</Link>
        </li>
        <li className="active">
          <Link to="/active">Active</Link>
        </li>
        <li className="completedTask">
          <Link to="/completed">Completed</Link>
        </li>
      </ul>
    </div>
  );
}

export default Navigation;
